module.exports = {
  /*
    Invisible reCaptcha site key
    Create one in https://www.google.com/recaptcha/intro/
  */
  RECAPTCHA_SITE_KEY: '6Lf5Dn4UAAAAAPzOghYEfCta33DmtXaqiO3hYbNI',

  // Google analytics tracking ID
  GOOGLE_ANALYTICS_ID: '',

  // Report email address
  REPORT_EMAIL: '',
};
