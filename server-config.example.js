module.exports = {
  PORT: process.env.KUTT_PORT,

  /* The domain that this website is on */
  DEFAULT_DOMAIN: process.env.KUTT_DOMAIN,

  /* Neo4j database credential details */
  DB_URI: process.env.KUTT_DB_URI || 'bolt://neo4j',
  DB_USERNAME: process.env.KUTT_DB_USERNAME || 'neo4j',
  DB_PASSWORD: process.env.KUTT_DB_PASSWORD || 'neo4j',

  /* A passphrase to encrypt JWT. Use a long and secure key. */
  JWT_SECRET: process.env.KUTT_JWT_SECRET || 'securekey',

  /*
    Invisible reCaptcha secret key
    Create one in https://www.google.com/recaptcha/intro/
  */
  RECAPTCHA_SECRET_KEY: process.env.RECAPTCHA_SECRET_KEY || '6Lf5Dn4UAAAAACwk2B3Ff0EV22wyimTOkdQtgtE1',

  /* 
    Google Cloud API to prevent from users from submitting malware URLs.
    Get it from https://developers.google.com/safe-browsing/v4/get-started
  */
  GOOGLE_SAFE_BROWSING_KEY: process.env.KUTT_GOOGLE_SAFE_BROWSING_KEY || '',

  /*
    Your email host details to use to send verification emails.
    More info on http://nodemailer.com/
  */
  MAIL_HOST: process.env.KUTT_MAIL_HOST || '',
  MAIL_PORT: process.env.KUTT_MAIL_PORT || 587,
  MAIL_SECURE: process.env.KUTT_MAIL_SECURE || false,
  MAIL_USER: process.env.KUTT_MAIL_USER || '',
  MAIL_PASSWORD: process.env.KUTT_MAIL_PASSWORD || '',

  REDIS_URL: process.env.REDIS_URL || 'redis://redis:6379'
};
