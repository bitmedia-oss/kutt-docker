const { promisify } = require('util');
const redis = require('redis');
const config = require('./config');

const client = redis.createClient({
    url: config.REDIS_URL
});

exports.get = promisify(client.get).bind(client);
exports.set = promisify(client.set).bind(client);
exports.del = promisify(client.del).bind(client);
