#!/bin/sh
rm -rf src
git clone -b v1.2.0 https://github.com/thedevs-network/kutt.git src
cp ./server-config.example.js ./src/server/config.js -v
cp ./client-config.example.js ./src/client/config.js -v
cd ./patch
cp . ../src -rv
cd ..